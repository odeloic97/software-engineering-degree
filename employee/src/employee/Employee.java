package employee;


public class Employee {
    private String firstName;
    private String lastName;
    private double salary;

    public Employee(String first, String last, double initialSalary){
        firstName = first;
        lastName = last;
        salary = initialSalary;

    }

    public void setFirstName(String first){
        firstName = first;
    }

    public String getFirstName(){
        return  firstName;
    }

    public void setLastName(String last){
        lastName = last;
    }

    public String getLastName(){
        return lastName;
    }


    public void setSalary(double initialSalary){
        if (initialSalary > 0.0)
            salary = initialSalary;
    }

    public double getSalary(){
        return salary;
    }

    public void raiseSalary() {
        salary = salary + (salary * 10) / 100;
    }

}
