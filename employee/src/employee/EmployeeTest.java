package employee;


//import  java.util.Scanner;
public class EmployeeTest {

    public static void  main(String[] args){
//        String firstName, lastName;
//        double salary;
//
//        // input user credentials
//
//        Scanner input = new Scanner(System.in);
//
//        System.out.println("\nEnter Employee first Name: ");
//        firstName = input.next();
//        System.out.println("\nEnter Employee Last Name: ");
//        lastName = input.next();
//        System.out.println("\nEnter Employee monthly Salary: ");
//        salary = input.nextDouble();

        Employee object1 = new Employee("Chris", "Leonardo", 212332.22);
        Employee object2 = new Employee("Karemera", "Aime", -122223.22);


        // This won't set the salary since the salary set is less than 0

        object1.setSalary(-210000.22);

        // But this will set the salary

        object2.setSalary(21213434.27);


        // initial data for Employee

        System.out.printf("\n%s, %s Salary is %f", object1.getFirstName(), object1.getLastName(), object1.getSalary());
        System.out.printf("\n%s, %s Salary is %f", object2.getFirstName(), object2.getLastName(), object2.getSalary());


        object1.raiseSalary();
        object2.raiseSalary();

        // After raise

        System.out.print("\nAfter Raise by 10%");
        System.out.printf("\n%s, %s Salary is %f", object1.getFirstName(), object1.getLastName(), object1.getSalary());
        System.out.printf("\n%s, %s Salary is %f", object2.getFirstName(), object2.getLastName(), object2.getSalary());


    }

}
