/**
 * Created by odeloic on 2/27/17.
 */
import java.util.Scanner;

public class Comparison {
    public static void main( String[] args ) {
        Scanner input = new Scanner(System.in);

        int number1;
        int number2;

        System.out.println("Enter the first Number : ");
        number1 = input.nextInt();

        System.out.print("\nEnter the second Number: ");
        number2 = input.nextInt();

        if (number1 > number2) {
            System.out.printf("\n%d is the biggest", number1);
        }

        else if (number1 < number2) {
            System.out.printf("\n%d is the biggest", number2);
        }

        else if (number1 == number2) {
            System.out.printf("\n%d and %d are equal", number1, number2);
        }

        else {
            System.out.printf("\n I am stuck!");
        }
    }

}

