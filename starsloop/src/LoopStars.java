/**
 * Created by odeloic on 3/31/17.
 */
public class LoopStars {
    public static void main( String args[]){
        for (int handler = 7; handler > 0; handler--){
            for (int innerHandler = 1; innerHandler <= handler; innerHandler++)
                System.out.print("*");
            System.out.println();
        }

    }
}
