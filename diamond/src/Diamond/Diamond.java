package Diamond;

/**
 * Created by odeloic on 4/7/17.
 */
public class Diamond {
    private int length;

    public Diamond(int diamondLength) {
        length = diamondLength;
    }

    public void setLength(int diamondLength) {
        length = diamondLength;

    }

    public int getLength() {
        return length;

    }
    public void drawDiamond() {
        int n = getLength();

        for (int i = 1; i < n; i += 2) {
            for (int firstHandler = n; firstHandler >= i; firstHandler -= 2) {
                System.out.print(" ");
            }
            for (int secondHandler = 1; secondHandler <= i; secondHandler++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int i = 1; i <= n; i += 2) {
            for (int firstHandler = 1; firstHandler <= i; firstHandler += 2) {
                System.out.print(" ");
            }
            for (int secondHandler = n; secondHandler >= i; secondHandler--) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
