package Diamond;

/**
 * Created by odeloic on 4/7/17.
 */

import java.util.Scanner;
public class diamondDrawTest {
    public static void main(String args[]){
        // We ask the user to enter any limit size for his diamond
        System.out.println("Hello, How big is your Diamond? Enter any digit");
        // this will hold our diamond size
        Scanner input = new Scanner(System.in);
        int size = input.nextInt();
        Diamond newDiamond = new Diamond(size);
        // Let's use our Diamond class setter method on our object
        newDiamond.setLength(size);
        newDiamond.drawDiamond();




    }
}
