package arrayOperations;

public class arrayTest {
    public static void main(String args[]) {
        int[][] a = {{0,1,2}, {3,4,5}, {6,7,8}, {9,10,11}};
        int [] [] b = {
                {12,6,2},
                {8,24,20},
                {16,7,0},
                {1,5,8}
        };

        arrayMethods newObject = new arrayMethods();

        newObject.showArray(b);


        System.out.printf("The total value of those elements is  : %d", newObject.getSum(b));
        System.out.printf("\nThe average of  those elements is  : %2f", newObject.getAverage(b));

        newObject.sortArrayDesc(b);
        newObject.showArray(b);

        newObject.reverseArray(a);
        newObject.showArray(a);

    }
}
