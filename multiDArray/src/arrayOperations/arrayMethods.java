package arrayOperations;

/**
 * Created by odeloic on 5/21/17.
 */
public class arrayMethods {

    /* First method: arrayShow()
     * returns void
     * takes one argument @array[][]
     */

    public void showArray(int a[][]) {
        System.out.println("\nThe Elements in the array : ");
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.printf("%d \t", a[i][j]);
            }
            System.out.println();
        }
    }

    /* Second method : getSum()
     * returns int value ( as a sum )
     * takes a multi dimension array as an argument
     */
    public int getSum(int a[][]) {
        int sum = 0;

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                sum += a[i][j];
            }
        }
        return sum;
    }

    /* Third method : getAverage()
     * returns the average of all array elements as double
     * takes a multi dimensional array as an argument
     */
    public double getAverage(int a[][]) {
        double average;
        int count = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++)
                count++;
        }
        average = getSum(a) / count;
        return average;
    }

    /* Fourth method : sortArrayDesc
     * sort elements of each row in descending order
     * takes an array as an argument
     */
    public void sortArrayDesc(int a[][]) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                for (int k = 0; k < a[i].length; k++) {
                    if (a[i][j] > a[i][k]) {
                        int temp = a[i][j];
                        a[i][j] = a[i][k];
                        a[i][k] = temp;
                    }
                }
            }
        }
    }

    /* Fourth method : sortArrayAsc
    * sort elements of each row in ascending order
    * takes an array as an argument
    */
    public void sortArrayAsc(int a[][]) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                for (int k = 0; k < a[i].length; k++) {
                    if (a[i][j] < a[i][k]) {
                        int temp = a[i][j];
                        a[i][j] = a[i][k];
                        a[i][k] = temp;
                    }
                }
            }
        }
    }

    //    public void searchValue(int value,int a[][]){}
    public void searchValue(int value, int anyArray[][]) {
        int foundOnRow = 0;
        int foundOnColumn = 0;
        for (int i = 0; i < anyArray.length; i++) {
            for (int j = 0; j < anyArray[i].length; j++) {
                if (anyArray[i][j] == value) {
                    foundOnRow = i;
                    foundOnColumn = j;
                    System.out.printf("%d found on %d th row and %d th column", value, foundOnRow, foundOnColumn);
                }

            }
        }


    }

    public void getIndex(int value, int anyArray[][]) {
        int foundOnRow = 0;
        int foundOnColumn = 0;
        for (int i = 0; i < anyArray.length; i++) {
            for (int j = 0; j < anyArray[i].length; j++) {
                if (anyArray[i][j] == value) {
                    foundOnRow = i;
                    foundOnColumn = j;
                    System.out.printf("\nElement found on : [%d][%d]", foundOnRow, foundOnColumn);
                }

            }
        }

//    public void reverseArray(int a[][]){}


    }

    public void reverseArray(int a[][]){
        for(int j = 0; j < a.length; j++){
            for(int i = 0; i < a[j].length / 2; i++) {
                int temp = a[j][i];
                a[j][i] = a[j][a[j].length - i - 1];
                a[j][a[j].length - i - 1] = temp;
            }
        }
    }
}
