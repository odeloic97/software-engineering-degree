package interest;


/**
 * Created by odeloic on 4/1/17.
 */

import java.util.Scanner;
public class InterestTest {
    public static void main( String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.println("what is your investment: ");
        double principal = input.nextDouble();
        int i = 1;
        double amount = 0;
        double rxn = 1.05;
        while (i <= 10) {

            amount = principal * Math.pow(1.05, i);

            i++;
        }

        System.out.printf("Your final interest is %f", amount);
    }

}
