package interest;

/**
 * Created by odeloic on 4/1/17.
 */
public class Person {
    private double investment;

    public Person(double initialInvestment) {
        investment = initialInvestment;

    }

    public void setInvestment(double initialInvestment) {
        investment = initialInvestment;
        }

    public double getInvestment(){
        return investment;
    }


    public double calculateInterest() {
        double amount = 0;
        double rate = 0.05;
        double rxn = 1 + rate;
        double investment = getInvestment();
        for (int i = 1; i <= 10; i++){
            for (int j = 1; j <= i; j++){
                rxn *= rxn;
            }

            amount += investment * rxn;
        }

        System.out.printf("\n your interest at the end of the quarter is %f", amount);
    return amount;

    }

}
