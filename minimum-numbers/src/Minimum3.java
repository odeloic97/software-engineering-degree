/**
 * Created by odeloic on 5/5/17.
 */


import java.lang.Math;
import java.util.Scanner;
public class Minimum3 {

    // Three numbers

    public static float getMinimumNumber(float num1, float num2, float num3) {
        float min;

        min = Math.min(num1, num2);
        min = Math.min(min, num3);


        return min;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float number1,number2,number3;
        float minimum;
        System.out.println("Enter three numbers: ");
        number1 = input.nextFloat();
        number2 = input.nextFloat();
        number3 = input.nextFloat();


        minimum =  getMinimumNumber(number1, number2, number3);

        System.out.printf("The minimum number from these 3 is : %f", minimum);
    }

}
