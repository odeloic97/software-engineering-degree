public class Perfect {

    public static void isPerfect(int number) { // function to check if the number is perfect, takes one argument, the number itself.
        int i; // will be used as the sentinel to loop through the @number's factors.
        int sum; // will be used to add all factors of @number itself.
        sum = 0; // we initialize the value of sum to 0.
        for (i = 1 ; i < number ; i++) {
            if (number % i == 0) // check if the current number is a factor to @number.
                sum += i; // we make a sum of all the factors of the number.
        } // end for

        if (number == sum) // we check if the result from the sum is equal to the number itself.
        {
            System.out.printf("\n%d is perfect.", number); // if the result is true, we print the number along with a confirmation sentence.
            System.out.printf(" its factors are: ");
            for ( i = 1 ; i < number ; i++) {
                if ( number % i == 0 ) {
                    System.out.printf(" %d", i);
                    System.out.print(",");
                }
            }
        }

   } // end isPerfect

    public static void main(String args[]){
        int x;

        for (x = 1; x < 1000 ; x++) // loop through all the numbers between 1 and 1000
            isPerfect(x); // call our function to check if the current number is perfect or not.

    } // end main
} // end class