package gas;

import java.util.Scanner;

public class Gas
{
    public void inputTripInfo()
    {
        Scanner input = new Scanner( System.in );

        int milesTravelled = 0;
        int gallonsUsedPerMile = 0;
        double mpg = 0.0;
        int total_milesTravelled = 0;
        int total_gallonsUsedPerMile = 0;
        double total_mpg = 0.0;
        int trips = 0;

        System.out.print( "Enter trip 1 mileage (as integer) or -1 to quit: ");
        milesTravelled = input.nextInt();
        if ( milesTravelled != -1 )
        {
            System.out.print( "Enter trip 1 gallons (as integer): ");
            gallonsUsedPerMile = input.nextInt();
            trips++;
        }

        while ( milesTravelled != -1 )
        {
            total_milesTravelled += milesTravelled;
            total_gallonsUsedPerMile += gallonsUsedPerMile;
            total_mpg = (double) total_milesTravelled / total_gallonsUsedPerMile;

            mpg = (double) milesTravelled / gallonsUsedPerMile;

            System.out.printf( "Trip %d's mpg (miles per gallon) is %.1f\n", trips, mpg );

            if ( trips > 1 )
            {
                System.out.printf( "\n   Total miles of your %d trips is %d\n", trips, total_milesTravelled );
                System.out.printf( "   Total gallons of your %d trips is %d\n", trips, total_gallonsUsedPerMile );
                System.out.printf( "   Combined mpg if your %d trips is %.1f\n", trips, total_mpg );
            }

            // Prompt the user for the next trip milesTravelled (possibly the sentinel)
            trips++;
            System.out.printf( "\nEnter trip %d mileage (as integer) or -1 to quit: ", trips );
            milesTravelled = input.nextInt();
            if ( milesTravelled != -1 )
            {
                System.out.printf( "Enter trip %d gallons (as integer): ", trips );
                gallonsUsedPerMile = input.nextInt();
            }
        }

        if ( total_milesTravelled != 0 )
        {
            System.out.printf( "\nFinal total miles driven is: %d\n", total_milesTravelled );
            System.out.printf( "Final total gallons used is: %d\n", total_gallonsUsedPerMile );
            System.out.printf( "Final combined mpg is: %.1f\n\n", total_mpg );
        }
        else
            System.out.printf( "No trip information was entered.\n\n" );

    } // end method getTripInfo
}

